package org.academiadecodigo.asynctomatics.scum;

@FunctionalInterface
public interface BiOperation<T> {

    T divide(T n1, T n2);
}
