package org.academiadecodigo.asynctomatics.scum;

@FunctionalInterface
public interface MonoOperation<T> {

    T multiply(T n1);
}
