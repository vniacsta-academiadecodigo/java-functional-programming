package org.academiadecodigo.asynctomatics.scum;

public class Machine {

    // the T in the angle brackets is to define the type
    public <T>T compute(T n1, MonoOperation<T> monoOperation) {

        return monoOperation.multiply(n1);
    }

    public <T>T compute(T n1, T n2, BiOperation<T> biOperation) {

        return biOperation.divide(n1, n2);
    }

}
