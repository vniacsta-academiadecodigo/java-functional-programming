package org.academiadecodigo.asynctomatics.fileanalyzerapp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileAnalyzer {

    public static void main(String[] args) {

        // init my class
        FileAnalyzer fileAnalyzer = new FileAnalyzer();

        // reading file 1
        System.out.println("\n  ** Analyzing song Audition lyrics **");

        // step 1 - counting words
        System.out.println("\nNumber of words: " + fileAnalyzer.readingCountingWords());

        // step 2 - finding first long word with the provided number of chars
        System.out.println("\nThe first longest word: " + fileAnalyzer.findingFirstLongestWord(8));

        // step 3 - finding the provided number of longest words
        System.out.println("\nLongest words are: " + fileAnalyzer.findingLongestWords(5));

        // comparing two files
        System.out.println("\n  ** Analyzing the songs Audition and Memories lyrics **");

        // step 4 - finding common words in the two files
        System.out.println("\nThe common words are: " + fileAnalyzer.commonWordsInFiles());
    }

    private Stream<String> initFile1() {

        try {

            return Files.lines(Paths.get("resources/audition.txt"))
                    .flatMap(line -> Stream.of(line.replaceAll("[^A-Za-z0-9' ]", "")
                            .split(" ")))
                    .map(String::toLowerCase);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private Stream<String> initFile2() {

        try {

            return Files.lines(Paths.get("resources/memories.txt"))
                    .flatMap(line -> Stream.of(line.replaceAll("[^A-Za-z0-9' ]", "")
                            .split(" ")))
                    .map(String::toLowerCase);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private long readingCountingWords() {

        return Objects.requireNonNull(initFile1())
                .count();
    }

    private String findingFirstLongestWord(int numChars) {

        return Objects.requireNonNull(initFile1())
                .filter(word -> word.length() >= numChars)
                .findFirst()
                .orElse("No words with " + numChars + " or more.");
    }

    private List<String> findingLongestWords(int limit) {

        return Objects.requireNonNull(initFile1())
                .sorted(Comparator.comparingInt(String::length).reversed())
                .distinct()
                .limit(limit)
                .collect(Collectors.toList());
    }

    private List<String> commonWordsInFiles() {

        return Objects.requireNonNull(initFile1())
                .filter(word -> Objects.requireNonNull(initFile2()).anyMatch(word::equals))
                .sorted(Comparator.comparingInt(String::length))
                .distinct()
                .collect(Collectors.toList());
    }
}
