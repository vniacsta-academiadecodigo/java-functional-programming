package org.academiadecodigo.asynctomatics.perfectnumbers;

import java.util.stream.IntStream;

public class PerfectNumbers {

    public static void main(String[] args) {

        PerfectNumbers pf = new PerfectNumbers();

        // timer to check how long does it take to perform an operation
        // starts here
        long start = System.currentTimeMillis();


        // if the number you provide is the same as the result, it's a perfect number
//        System.out.println(pf.sumDivisors(6));

        // check perfect numbers within a range
//        pf.checkPerfect(1, 1000);

        // check perfect numbers between 1 and the number you provide
//        System.out.println(pf.sumDivisorsBetter(6));

        // check perfect numbers between 1 and the number you provide
        // more efficient with sumDivisorsBetter()
        // trying .parallel() method
        // ATTENTION: it will take a while and lots of your CPU power xD
        pf.checkPerfectBetter(6);


        // timer ends here
        double elapsed = (System.currentTimeMillis() - start) / 1000;
        System.out.println("time elapsed (sec): " + elapsed);
    }

    public int sumDivisors(int num) {

        return IntStream.range(1, num)
                .filter(x -> num % x == 0)
                .sum();
    }

    public void checkPerfect(int min, int max) {

        IntStream.range(min, max)
                .filter(x -> sumDivisors(x) == x)
                .forEach(System.out::println);
    }

    public int sumDivisorsBetter(int n) {

        int root = (int) Math.sqrt(n);
        int sum = IntStream.rangeClosed(2, root)
                .filter(x -> n % x == 0)
                .map(x -> x + n / x)
                .sum();

        if (n == root * root) {
            sum = sum - root;
        }

        return sum + 1;
    }

    public void checkPerfectBetter(int limit) {

        IntStream.iterate(1, n -> n + 1)
                .filter(x -> x == sumDivisorsBetter(x))
                .parallel()
                .limit(limit)
                .forEach(System.out::println);
    }

}
